const sharp = require("sharp")
const path = require('path')
const fs = require('fs')
const _ = require('lodash')

const getTextRow = (line) => `<tspan x="0" dy="1.2em">${line}</tspan>`
const getLines = (text) => text.split(/\n/)

const changeTextInSvg = async (svgPath, text) => {
    let body = fs.readFileSync(svgPath, { encoding: 'utf-8' })
    const lines = getLines(text)
    const xmlLines = lines.map(getTextRow)
    body = body.replace(/\{\{target-text-place\}\}/gm, xmlLines.join('\n'))
    const changedSvgPath = path.resolve(__dirname, './tmp', `${_.uniqueId()}_tmp.svg`)
    fs.writeFileSync(changedSvgPath, body)

    return changedSvgPath
}

const getQuizeImage = async (text) => {
    const svgPath = path.resolve(__dirname, "../images/source.svg")
    const outputPath = path.resolve(__dirname, `../../output/${_.uniqueId()}.png`)
    const changedSvgPath = await changeTextInSvg(svgPath, text)

    try {
        await sharp(changedSvgPath)
            .png()
            .toFile(outputPath)
    } catch (error) {
        console.error('error', error)
    }

    fs.unlink(changedSvgPath, () => {})
    return outputPath
}

module.exports = getQuizeImage
