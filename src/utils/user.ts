import TelegramBot from 'node-telegram-bot-api';

type getUserDataFromMessageType = (msg: TelegramBot.Message) => TelegramBot.Chat | TelegramBot.User;
type getUserIdFromMessageType = (msg: TelegramBot.Message) => number;

export const getUserDataFromMessage: getUserDataFromMessageType = (msg) =>
    msg.chat || msg.from;

export const getUserIdFromMessage: getUserIdFromMessageType = (msg) =>
    getUserDataFromMessage(msg).id;
