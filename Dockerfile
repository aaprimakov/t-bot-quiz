FROM 'node:14'

RUN mkdir -p /usr/src/app/src/
WORKDIR /usr/src/app/

COPY ./src /usr/src/app/src
COPY ./images /usr/src/app/images
COPY ./package.json /usr/src/app/package.json
COPY ./tsconfig.json /usr/src/app/tsconfig.json
COPY ./.env /usr/src/app/.env

RUN npm install --only=prod -d
EXPOSE 8090

CMD ["npm", "run", "start:prod"]
